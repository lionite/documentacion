.. Omnileads Installation documentation master file, created by
   sphinx-quickstart on Wed Apr 30 10:52:05 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentacion de Instalacion de Omnileads
=========================

Arquitectura
------------

.. image:: arquitectura.png


Acceso al sistema
-----------------

El sistema puede ser accedido utilizando la URL http://hostname.domain

.. note::

    El acceso web a OMniLeads debe ser a través del hostname.domain del nodo. Por lo tanto existen dos posibilidades a la hora de resolver el hostname:

    - Que los DNS de la red lo hagan.
    - Que se añada el hostname.domain dentro del archivo de hosts (Windows, Linux o Mac).

Usuarios
--------

El sistema funciona bajo el usuario ``omnileads``. La aplicacion se encuentra en el directorio ``/opt/omnileads/``:

.. code::

    omnileads@omnileads:~/ominicontacto$ ls -lh /opt/omnileads/
    total 72K
    drwxr-xr-x  1 omnileads omnileads 4.0K Feb 25 17:30 asterisk
    drwxr-xr-x  2 omnileads omnileads 4.0K Feb 25 16:56 backup
    drwxr-xr-x  2 omnileads omnileads 4.0K Feb 25 17:38 bin
    drwxr-xr-x  1 omnileads omnileads 4.0K Feb 25 17:37 kamailio
    drwxr-xr-x  2 omnileads omnileads 4.0K Feb 25 16:57 local
    drwxr-xr-x  1 omnileads omnileads 4.0K Feb 25 17:37 log
    drwxr-xr-x  1 omnileads omnileads 4.0K Feb 26 09:37 media_root
    -rw-r--r--  1 root      root      2.7K Feb 25 17:41 my_inventory
    drwxr-xr-x  2 omnileads omnileads 4.0K Feb 25 17:38 nginx_certs
    drwxrwxr-x 15 omnileads omnileads 4.0K Mar  7 10:12 ominicontacto
    lrwxrwxrwx  1 omnileads omnileads   37 Feb 25 17:38 Omnisup -> /opt/omnileads//ominicontacto/Omnisup
    drwxr-xr-x  2 omnileads omnileads 4.0K Feb 25 17:38 run
    drwxr-xr-x 12 omnileads omnileads 4.0K Feb 25 17:38 static
    drwxr-xr-x  7 omnileads omnileads 4.0K Feb 25 16:56 virtualenv
    drwxr-xr-x  2 omnileads omnileads 4.0K Feb 25 16:56 wombat-json

A continuación se menciona el contenido de las carpetas:

* **asterisk:** ubicación de instalación de asterisk.
* **backup:** backups creados mediante script backup-restore.sh
* **bin:** scripts para sysadmin.
* **kamailio:** ubicación de instalación de todos los archivos referentes a kamailio.
* **local:** archivos de settings de django
* **log:** logs de django y uwsgi
* **media_root:** archivos de media (audios subidos al sistema y csv de reportes)
* **nginx_certs:** certificados usados por nginx
* **ominicontacto:** código django de omnileads (o repositorio clonado)
* **Omnisup:** código de la supervisión
* **run:** archivos .ini, .pid y .socket usados pow uwsgi
* **static:** archivos estáticos generados luego de un collectstatic, entiendase frontend (js, css y bootstrap)
* **virtualenv:** paquetería necesaria para correr la aplicación
* **wombat-json:** archivos .json generados para interactuar con API de wombat dialer

Logs
--------

- En el directorio ``/opt/omnileads/log`` se encuentran 3 archivos de log correspondientes a django y uwsgi:

.. code::

    [root@centos-example ~]# ls -lh /opt/omnileads/log/
    total 132K
    -rwxr-xr-x. 1 omnileads omnileads 55K mar  6 16:07 django.log
    -rwxr-xr-x  1 omnileads omnileads 61K mar  6 15:21 ominicontacto_app.log
    -rwxr-xr-x. 1 omnileads omnileads 11K mar  7 18:00 uwsgi.log

* En ``django.log`` se encuentran los logs de la aplicación.
* En ``uwsgi.log`` se encuentran los logs del Emperor Uwsgi.
* En ``ominicontacto_app.log`` se encuentran los logs del Vassal Uwsgi.

- En el directorio ``/opt/omnileads/asterisk/var/log/asterisk`` están dos archivos de log:

.. code::

    [root@centos-example ~]# ls -lh /opt/omnileads/asterisk/var/log/asterisk/
    total 140K
    -rwxr-xr-x. 1 omnileads omnileads 1,9K mar  6 16:07 agis-errors.log
    -rw-r--r--  1 omnileads omnileads  277 mar  7 18:25 full

* En ``agis-errors.log`` se encuentran los logs los AGI para Omnileads.
* En ``full`` se tiene el log convencional de  asterisk

- En el directorio ``/var/log/kamailio/kamailio.log`` está el log DEBUG de kamailio y en  ``/var/log/rtpengine/rtpengin.log`` el de rtpengine, también se puede observar el log de kamailio con menos DEBUG en ``/var/log/messages``
- En el directorio ``/var/lib/pgsql/9.6/data/pg_log/`` están los logs de postgresql (Solo para CentOS)

Servicios
---------

El deploy del sistema incluye el setup de 4 servicios (Asterisk, Omnileads, Kamailio y Rtpengine):

.. code::

    service asterisk status o systemctl status asterisk (se puede hacer stop, start, restart y reload)
    service omnileads status o systemctl status omnileads (se puede hacer stop, start, restart y reload)
    service kamailio status o systemctl status kamailio (se puede hacer stop, start y restart)
    service kamailio status o systemctl status kamailio (se puede hacer stop, start y restart)

A su vez el sistema cuenta con otros servicios cuyo setup es inherente a la instalación de cada servicio

* Databases: postgresql, redis, mariadb
* Webservers: tomcat8, nginx
* CGI: uwsgi y php-fpm




Contents
--------

.. toctree::
   :maxdepth: 2
   :glob:

   deploy
   inventory
   deploy_elastix3
   procesos
   release_notes
   release_notes_old
   fts_web_*
   fts_daemon_*


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
