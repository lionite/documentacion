.. highlight:: bash


Deploy
======

El deploy comienza con la ejecución del script deploy.sh ubicado en ``deploy/ansible`` , dentro del repositorio
este script bash hace lo siguiente:

* Valida el distro donde se está ejecutando (puede ser Debian, Ubuntu o CentOS)
* Instala paquetería necesaria que para instalar ansible y paquetería esencial que no viene por defecto en algunas distros. Ej: sudo en debian
* Valida que el script se está ejecutando con usuario root o con sudo.
* Recibe como primer argumento las opciones listadas en el help
* Recibe como segundo argumento -a o -c, dependiendo si se va a instalar AIO o cluster horizontal
* Puede recibir o no como tercer argumento -d que establece que se va a instalar un devenv
* Ejecuta el script keytransfer.sh, el cual se encarga de transferir la llave pública del usuario donde se ejecuta ansible
  hacia los servidores a instalar (en caso de ser instalación host-node se hablará de esto después)
* Detecta si ansible está instalado en /root/.local si no, lo instala
* Copia el código del repositorio y el playbook y lo ubica en un directorio temporal
* Genera datos de la version que se está instalando para el archivo version.py
* Ejecuta el comando ansible-playbook para iniciar la ejecución del playbook

.. note::

  La versión de Ansible usada hasta el momento (Marzo de 2019) es la 2.5.0

.. caution::

  **Known Issue:** hasta release-1.2.0 este script tiene que ser ejecutado con el usuario root a la primera vez. Luego si puede
  ser ejecutado por el superusuario

Ansible: Modos de instalación
----------------

**Self-Hosted** es decir se ejecuta ansible en el mismo nodo donde se va a instalar Omnileads.

.. image:: imagenes/selfhosted.png
  :align: center

**Host-node** es decir, se ejecuta ansible desde un workstation (host) aparte del server a instalar (node). Para ello ansible usa
ssh para pasar los comandos a ejecutar, por ello es que en el script de deploy se transfieren la llave pública del host a cada nodo.

.. image:: imagenes/hostnode.png
  :align: center

.. note::

  Para iniciar un estudio de profundo de ansible, remitirse a la documentación oficial: https://docs.ansible.com/ansible/2.5/user_guide/intro_getting_started.html

Archivo de inventario
----------------------

El archivo de inventario contiene los datos de los nodos a instalar ya sea para AIO o cluster, tiene variables dinámicas, osea
que pueden ser modificadas por el usuario (contraseñas y usernames de los servicios).

Inventario para una una instalación AIO
########

.. code::

  [omnileads-aio]
  hostname ansible_connection=local ansible_user=root ansible_host=X.X.X.X  #(this line is for self-hosted installation)
  #hostname ansible_ssh_port=22 ansible_user=root ansible_host=X.X.X.X #(this line is for node-host installation)

* Se modifica la linea con ``ansible_connection=local`` para una instalación self-hosted. Aqui es necesario cambiar hostname por el nombre de dominio del servidor a instalar y las X cambiarlas por la IP.
* Se modifica la linea con ``ansible_ssh_port=22`` para una instalación host-node. También es necesario cambiar hostname e IP. Se pueden agregar cuantas lineas de esta instalación se quiera.

.. note::

  Se pueden agregar los nodos que se quieran para instalaciones host-node

Inventario para una instalación cluster horizontal
########

En primer lugar contiene los nodos usados para un cluster horizontal (asterisk, kamailio, omniapp, database,dialer)

.. code::

    [omniapp]
    omniapp ansible_ssh_port=22 ansible_user=root ansible_host=X.X.X.X #cluster server do not erase this comment
    [kamailio]
    kamailio ansible_ssh_port=22 ansible_user=root ansible_host=X.X.X.X #cluster server do not erase this comment
    [asterisk]
    asterisk ansible_ssh_port=22 ansible_user=root ansible_host=X.X.X.X #cluster server do not erase this comment
    [database]
    database ansible_ssh_port=22 ansible_user=root ansible_host=X.X.X.X #cluster server do not erase this comment
    [dialer]
    dialer ansible_ssh_port=22 ansible_user=root ansible_host=X.X.X.X #cluster server do not erase this comment

Basta con cambiar hostname, puerto ssh e IP por los valores configurados en cada nodo a instalar

.. caution::

  **Known Issue:** hasta el momento solo se puede instalar de a un solo cluster horizontal a la vez

Luego se establece que habrá un grupo llamado omnileads-cluster que tendrá de hijos los componentes del cluster

.. code::

  [omnileads-cluster:children]
  omniapp
  database
  asterisk
  kamailio
  dialer

Variables
########

Hay variables usadas para la instalación AIO / Cluster, variables usadas solo para AIO y solo para el cluster.
Primero se establece el grupo everyone que tendrá como hijos a omnileads-aio y omnileads-cluster

.. code::

    [everyone:children]
    omnileads-aio
    omnileads-cluster

Se setean las variables para everyone

.. code::

    [everyone:vars]
    install_prefix=/opt/omnileads/

    # Change this variables values as you want
    postgres_password=admin123
    admin_pass=098098ZZZ
    formato_conversion=mp3

    #Change this value if you want to change the language of schedule calification
    schedule=Agenda

    # Do not change this variables unless you know what you are doing
    ami_user=omnileadsami
    ami_password=5_MeO_DMT
    postgres_database=omnileads
    postgres_user=omnileads
    dialer_user=demoadmin
    dialer_password=demo
    mysql_root_password=098098ZZZ
    sup_ami_user=omnisup
    sup_ami_password=Sup3rst1c10n

Variables para aio

.. code::

    [omnileads-aio:vars]
    ################ Puerto SSH ##################
    ssh_port="{{ ansible_ssh_port }}"
    ###### IP's y FQDN ########
    omni_ip="{{ ansible_host }}"
    omniapp_ip="{{ omni_ip }}"
    asterisk_ip="{{ omni_ip }}"
    kamailio_ip="{{ omni_ip }}"
    database_ip="{{ omni_ip }}"
    dialer_ip="{{ omni_ip }}"
    omni_fqdn="{{ inventory_hostname }}"
    omniapp_fqdn="{{ omni_fqdn }}"
    asterisk_fqdn="{{ omni_fqdn }}"
    kamailio_fqdn="{{ omni_fqdn }}"
    database_fqdn="{{ omni_fqdn }}"
    dialer_fqdn="{{ omni_fqdn }}"

Y variables para cluster

.. code::

    [omnileads-cluster:vars]
    ###### IP's y FQDN ########
    omniapp_ip="{{ hostvars[groups['omniapp'][0]].ansible_host }}"
    omniapp_fqdn="{{ hostvars[groups['omniapp'][0]].inventory_hostname }}"
    database_ip="{{ hostvars[groups['database'][0]].ansible_host }}"
    database_fqdn="{{ hostvars[groups['database'][0]].inventory_hostname }}"
    asterisk_ip="{{ hostvars[groups['asterisk'][0]].ansible_host }}"
    asterisk_fqdn="{{ hostvars[groups['asterisk'][0]].inventory_hostname }}"
    kamailio_ip="{{ hostvars[groups['kamailio'][0]].ansible_host }}"
    kamailio_fqdn="{{ hostvars[groups['kamailio'][0]].inventory_hostname }}"
    dialer_ip="{{ hostvars[groups['dialer'][0]].ansible_host }}"
    dialer_fqdn="{{ hostvars[groups['dialer'][0]].inventory_hostname }}"

.. note::

  Se puede optimizar e incluso usar multiples archivos de inventario, para mas info: https://docs.ansible.com/ansible/2.5/user_guide/intro_inventory.html

Setup del nodo en self-hosted mode
-------------------------

Las distribuciones de GNU/Linux soportadas para Omnileads son:
* Ubuntu Server 18.04 LTS
* Debian Stretch 9
* Centos 7.6
A su vez se ha experimentado instalar en:
* Fedora 28

Para Ubuntu y Debian hay que hacer un par de pasos previos a la ejecución del script de deploy:

* Instalar sudo, openssh-server y python-minimal, si no se tiene instalado:

.. code::

  apt-get install sudo openssh-server python-minimal -y

* Permitir login SSH con el usuario root:

.. code::

  sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

* Restart del servicio ssh

.. code::

  service ssh restart
